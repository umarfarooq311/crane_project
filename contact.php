﻿<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from templates.scriptsbundle.com/crane-template/demos/contact.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 08:23:24 GMT -->
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!--[if IE]>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="author" content="ScriptsBundle">
    <title>Crane Construction Template</title>
    <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/style.css">
    <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">
    <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/et-line-fonts.css" type="text/css">
    <!-- =-=-=-=-=-=-= Animation =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">
    <!-- =-=-=-=-=-=-= Menu Drop Down =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/bootstrap-dropdownhover.min.css" type="text/css">
    <!-- =-=-=-=-=-=-= Revolution Main Stylesheet =-=-=-=-=-=-= -->
    <link href="js/revolution/css/settings.css" rel="stylesheet" media="screen" />
    <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.style.css">
    <!-- =-=-=-=-=-=-= Google Fonts =-=-=-=-=-=-= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic,900italic,900,300,300italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
    <!-- =-=-=-=-=-=-= Magnific PopUP CSS =-=-=-=-=-=-= -->
	<link href="js/magnific-popup/magnific-popup.css" rel="stylesheet">
	<!-- =-=-=-=-=-=-= Flaticons CSS =-=-=-=-=-=-= -->
	<link href="css/flaticon.css" rel="stylesheet">
    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="js/revolution/css/settings.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->
   

    <!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->
    <div class="header-top clear">
        <div class="container">
            <div class="row">
                <div class="col-md-7 hidden-sm hidden-xs">
                    <div class="header-top-left header-top-info">
                        <p><a href="tel:+3211234567"><i class="fa fa-phone"></i> 416-676-5555</a></p>
                         <p><a href="mailto:contact@scriptsbundle.com"><i class="fa fa-envelope"></i>discounttowing@hotmail.com</a></p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="header-top-right pull-right">
                        <ul>
                           <!--  <li class="search-bar">
                                <div class="search-form">
                                    <form action="#" method="get">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                        <input type="text" name="search" id="search" placeholder="Search information">
                                    </form>
                                </div>
                            </li> -->
                           <!--    <div class="social-links-two clearfix">
                         <a class="facebook img-circle" href="#"><span class="fa fa-facebook-f"></span></a>
                         <a class="twitter img-circle" href="#"><span class="fa fa-twitter"></span></a>
                         <a class="google-plus img-circle" href="#"><span class="fa fa-google-plus"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-pinterest-p"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-linkedin"></span></a>
                        </div> -->
                          <!--   <li class="cart-active">
                                <div class="cart-btn">
                                    <a href="#"><i class="fa fa-shopping-cart"></i><sup>2</sup></a>
                                </div>
                                <div class="cart-grid">
                                    <div class="cart-grid-box">
                                        <div class="cart-grid-box-img">
                                            <img class="img-responsive" src="images/shop/cart.png" alt="">
                                        </div>
                                        <div class="cart-grid-box-title">
                                            <a href="#"> Speed Orbital Jigsaw </a>
                                            <p class="price"> $215.00</p>

                                        </div>
                                        <div class="cart-grid-box-del">
                                            <a href="#"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="cart-grid-box">
                                        <div class="cart-grid-box-img">
                                            <img class="img-responsive" src="images/shop/cart.png" alt="">
                                        </div>
                                        <div class="cart-grid-box-title">
                                            <a href="#"> Speed Orbital Jigsaw  </a>
                                            <p class="price"> $215.00</p>
                                        </div>
                                        <div class="cart-grid-box-del">
                                            <a href="#"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="total-amount fix">
                                        <p>SUBTOTAL: </p><span>$330.00 </span>

                                    </div>
                                    <div class="action-cart">
                                        <a href="#" class="viewcart">View Cart</a>
                                        <a href="#" class="checkout">Check Out</a>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Top End  -->

    <!-- Header Area -->
    <header class="header-area">

        <!-- Logo Bar -->
        <div class="border">
            <!-- Logo Bar -->
            <div class="logo-bar">
                <div class="container clearfix">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="index.php"><img src="images/demo5.png" alt=""></a>
                    </div>

                    <!--Info Outer-->
                     <div class="information-content">
                        <!--Info Box-->
                        <div class="info-box  hidden-sm">
                            <div class="icon"><span class="icon-envelope"></span></div>
                            <div class="text">Email</div>
                            <a href="mailt:contact@scriptsbundle.com">discounttowing@hotmail.com</a> </div>
                        <!--Info Box-->
                        <div class="info-box">
                            <div class="icon"><span class="icon-phone"></span></div>
                            <div class="text">Call Now</div>
                            <a class="location" href="#"> 416-676-5555</a> </div>
                        <div class="info-box">
                            <div class="icon"><span class="icon-map-pin"></span></div>
                            <div class="text">Address</div>
                            <a class="location" href="#">23 CreditStone Rd, Concord, ON L4K 1N4, CA </a> </div>
                    </div>
                </div>
            </div>
            <!-- Header Top End -->
        </div>
        <!-- Header Top End -->

        <!-- Menu Section -->
        <div class="navigation-2">
            <!-- navigation-start -->
            <nav class="navbar navbar-default ">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="main-navigation">
                        <ul class="nav navbar-nav">
                           <li><a href="index.php">Home</a></li>
                             <li><a href="about.php">About </a> </li>
                               <li><a href="services.php">Services</a> </li>
                            </li>
                              <li><a href="equipment.php">Equipment</a> </li>
                              <li class="active"><a href="contact.php">Contact us</a></li>
                           
                        </ul>
                        <a href="contact.php" class="btn btn-primary pull-right">Request A Quote</a> </div>
                    <!-- /.navbar-collapse -->

                </div>
                <!-- /.container-end -->
            </nav>
        </div>
        <!-- /.navigation-end -->
        <!-- Menu  End -->
    </header>
    <!-- =-=-=-=-=-=-= HEADER END =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Page Breadcrumb =-=-=-=-=-=-= -->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <!-- end col -->
                <div class="col-md-12 col-sm-12 text-left">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Pages</a></li>
                            <li><a href="#" class="active">Contact Us</a></li>
                        </ol>
                    </div>
                    <!-- end bread -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Page Breadcrumb End =-=-=-=-=-=-= -->
<div class="container">
<div class="main-heading text-center">
                <h2>Contact Us</h2>
            </div>
        </div>
    <!-- =-=-=-=-=-=-= Google Map =-=-=-=-=-=-= -->
    <section id="google-map">
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2880.212695015829!2d-79.517697784251!3d43.789199379116916!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b2fb4ab547c0f%3A0x3c8ac852138dbff9!2s23+Creditstone+Rd%2C+Concord%2C+ON+L4K+1N4%2C+Canada!5e0!3m2!1sen!2s!4v1560860555819!5m2!1sen!2s" width="1270" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- container-fluid end -->
    </section>

    <!-- =-=-=-=-=-=-= Google Map End =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Contact Us =-=-=-=-=-=-= -->
    <section id="contact-us" class="section-padding-80 contact-bg">
        <div class="container">
            <!-- Row -->
            <div class="row">





                <div class="col-md-7  ">


                    <div class="about-title">
                        <h2>Leave A Message</h2>
                      



                    </div>

                    <div class="notice success" id="success">
                        <p>Thanks so much for your message. We check e-mail frequently and will try our best to respond to your inquiry.</p>
                    </div>

                    <div class="row margin-top-50">
                        <form id="contactForm" method="POST" action="mailer.php" >
                            <div class="col-sm-6">
                                <!-- Name -->
                                <div class="form-group">

                                    <input type="text" placeholder="Name" id="name" name="name" class="form-control" required>
                                </div>
                            </div>
                            <!-- End col-sm-6 -->

                            <div class="col-sm-6">
                                <!-- Email -->
                                <div class="form-group">

                                    <input type="email" placeholder="Email" id="email" name="email" class="form-control" required>
                                </div>
                            </div>
                            <!-- End col-sm-6 -->


                            <div class="col-sm-12">
                                <!-- Email -->
                                <div class="form-group">

                                    <input type="text" placeholder="Subject" id="subject" name="subject" class="form-control" required>
                                </div>
                            </div>
                            <!-- End col-sm-12 -->


                            <div class="col-sm-12">
                                <!-- Comment -->
                                <div class="form-group">

                                    <textarea placeholder="Message..." id="message" name="message" class="form-control" rows="6" required></textarea>
                                </div>
                            </div>
                            <!-- End col-sm-12 -->

                            <div class="col-sm-12">
                    <input type="submit" id="yes" name="submit" class="btn btn-primary" onsubmit="return formsubmited()">
                                <img id="loader" alt="" src="images/loader.gif" class="loader">
                            </div>
                            <!-- End col-sm-6 -->

                        </form>
<script type="text/javascript">
    



    
</script>


                    </div>

                </div>

                <div class="col-md-5">

                    <div class="contact-image">
                        <img alt="" src="images/contact.png">
                    </div>
                </div>



                <div class="clearfix"></div>
            </div>
            <!-- Row End -->
        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Contact Us End =-=-=-=-=-=-= --><!-- =-=-=-=-=-=-= Our Clients =-=-=-=-=-=-= -->
  <div class="parallex-small ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="parallex-text">
                        <h4>Not sure which solution fits you business needs?</h4>
                    </div>
                    <!-- end subsection-text -->
                </div>
                <!-- end col-md-8 -->

                <div class="col-md-4 col-sm-4">
                    <div class="parallex-button"> <a href="contact.php" class="btn btn-lg btn-clean">Contact Us <i class="fa fa-angle-double-right "></i></a> </div>
                    <!-- end parallex-button -->
                </div>
                <!-- end col-md-4 -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- =-=-=-=-=-=-= Our Clients -end =-=-=-=-=-=-= -->




    <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
    <footer>
        <div class="footer custom-padding">
            <div class="container">
                <div class="row">
                   <div class="col-md-6 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">About us</h3>
                        
                        <p>Discount Crane Service (A Division of Dacapa Crane & Rigging Ltd.) was established in Alberta in 1996 to provide hoisting, rigging, lift planning and transportation services for the mining, oil & gas, construction and power generation industries.</p>
                        <div class="social-links-two clearfix">
                         <a class="facebook img-circle" href="#"><span class="fa fa-facebook-f"></span></a>
                         <a class="twitter img-circle" href="#"><span class="fa fa-twitter"></span></a>
                         <a class="google-plus img-circle" href="#"><span class="fa fa-google-plus"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-pinterest-p"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-linkedin"></span></a>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-2 col-sm-6 col-xs-12 clearfix column">
                      <!--   <h3 class="footer-title">Quick links</h3>
                        <div class="footer-links">
                            <ul>
                                
                                <li><a href="index.php">Home</a></li>
                                
                                <li><a href="about.php">About</a></li>
                                
                                <li><a href="services.php">Service</a></li>
                                
                                <li><a href="equipment.php">Equipment</a></li>
                               
                                <li><a href="contact.php">Contact</a></li>
                                
                            </ul>
                        </div> -->
                    </div>
                    <!-- /.col -->
                   <!--  <div class="col-md-3 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">Flickr widget</h3>
                        <div class="footer-gallery-grid">
                            <a href="#">
                                <img alt="" src="images/blog/small-1.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-2.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-3.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-4.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-5.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-6.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-7.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-8.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-9.png">
                            </a>
                            
                       
                        </div>
                    </div> -->
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">Contact us</h3>
                        <div class="footer-address">
                            <p><i class="fa fa-map-marker"></i>23 CreditStone Rd, Concord, ON L4K 1N4, CA</p>
                            <p><i class="fa fa-envelope"></i>discounttowing@hotmail.com</p>
                            <p><i class="fa fa-phone"></i>416-676-5555</p>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.footer -->
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                             <p>Copyright &copy; 2019 Discount Crane &nbsp;</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<!-- =-=-=-=-=-=-= FOOTER END =-=-=-=-=-=-= -->
    
    
    <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap Core Css  -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Menu Hover  -->
    <script src="js/hover.min.js"></script>
    <!-- Jquery Smooth Scroll  -->
    <script src="js/jquery.smoothscroll.js"></script>
    <!-- Jquery Easing -->
    <script type="text/javascript" src="js/easing.js"></script>
    <!-- Jquery Counter -->
    <script src="js/jquery.countTo.js"></script>
    <!-- Jquery Waypoints -->
    <script src="js/jquery.waypoints.js"></script>
    <!-- Jquery Appear Plugin -->
    <script src="js/jquery.appear.min.js"></script>
    <!-- Jquery Shuffle Portfolio -->
    <script src="js/jquery.shuffle.min.js"></script>
    <!-- Carousel Slider  -->
    <script src="js/carousel.min.js"></script>
    <!-- Jquery Parallex -->
    <script src="js/jquery.stellar.min.js"></script>
    
    <!-- jQuery REVOLUTION Slider  -->
    <script src="js/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="js/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Gallery Magnify  -->
    <script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Sticky Bar  -->
    <script src="js/theia-sticky-sidebar.js"></script>
    <!-- Template Core JS -->
    <script src="js/custom.js"></script>

    <!-- Google Map For This Page Only -->

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVj6yChAfe1ilA4YrZgn_UCAnei8AhQxQ&amp;sensor=false"></script>
    <script type="text/javascript" src="js/map.js"></script>
    <script type="text/javascript" src="js/validator.min.js"></script>
    <script type="text/javascript">
        (function($) {
            "use strict";

            $('#contactForm').validator().on('submit', function(e) {
                if (e.isDefaultPrevented()) {
                    // handle the invalid form...
                } else {
                    $("#yes").html('Processsing...');
                    $('#loader').show();
                    $.ajax({
                        type: "POST",
                        url: 'contact.php',
                        data: $("#contactForm").serialize(), //only input

                        success: function(response) {
                            if (response == "success") {
                                $('#success').show();
                                $('#loader').hide();
                                $("#yes").html('Done Email...');
                                $('#contactForm').each(function() {
                                    this.reset();
                                });

                            }
                        }
                    });
                    return true;
                }
            })

        })(jQuery);
    </script>

</body>


<!-- Mirrored from templates.scriptsbundle.com/crane-template/demos/contact.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 08:23:49 GMT -->
</html>







