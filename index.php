﻿<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from templates.scriptsbundle.com/crane-template/demos/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 08:12:41 GMT -->
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!--[if IE]>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="author" content="ScriptsBundle">
    <title>Crane Construction Template</title>
    <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/style.css">
    <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">
    <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/et-line-fonts.css" type="text/css">
    <!-- =-=-=-=-=-=-= Animation =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">
    <!-- =-=-=-=-=-=-= Menu Drop Down =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/bootstrap-dropdownhover.min.css" type="text/css">
    <!-- =-=-=-=-=-=-= Revolution Main Stylesheet =-=-=-=-=-=-= -->
    <link href="js/revolution/css/settings.css" rel="stylesheet" media="screen" />
    <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.style.css">
    <!-- =-=-=-=-=-=-= Google Fonts =-=-=-=-=-=-= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic,900italic,900,300,300italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
    <!-- =-=-=-=-=-=-= Magnific PopUP CSS =-=-=-=-=-=-= -->
	<link href="js/magnific-popup/magnific-popup.css" rel="stylesheet">
	<!-- =-=-=-=-=-=-= Flaticons CSS =-=-=-=-=-=-= -->
	<link href="css/flaticon.css" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Flaticons CSS =-=-=-=-=-=-= -->
    <link href="css/flaticon.css" rel="stylesheet">
    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->
    <div class="preloader"></div>

    <!-- Header Area -->
    <header class="transparent-header sticky">
<div class="header-top clear">
        <div class="container">
            <div class="row">
                <div class="col-md-7 hidden-sm hidden-xs">
                    <div class="header-top-left header-top-info">
                        <p><a href="tel:+3211234567"><i class="fa fa-phone"></i> 416-676-5555</a></p>
                         <p><a href="mailto:contact@scriptsbundle.com"><i class="fa fa-envelope"></i>discounttowing@hotmail.com</a></p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="header-top-right pull-right">
                        <ul>
                            <!-- <li class="search-bar"> -->
                                <!-- <div class="search-form">
                                    <form action="#" method="get">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                        <input type="text" name="search" id="search" placeholder="Search information">
                                    </form>
                                </div> -->
                              <!--   <div class="social-links-two clearfix">
                         <a class="facebook img-circle" href="#"><span class="fa fa-facebook-f"></span></a>
                         <a class="twitter img-circle" href="#"><span class="fa fa-twitter"></span></a>
                         <a class="google-plus img-circle" href="#"><span class="fa fa-google-plus"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-pinterest-p"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-linkedin"></span></a>
                        </div> -->
                           <!--  </li> -->
                           <!--  <li class="cart-active">
                                <div class="cart-btn">
                                    <a href="#"><i class="fa fa-shopping-cart"></i><sup>2</sup></a>
                                </div>
                                <div class="cart-grid">
                                    <div class="cart-grid-box">
                                        <div class="cart-grid-box-img">
                                            <img class="img-responsive" src="images/shop/cart.png" alt="">
                                        </div>
                                        <div class="cart-grid-box-title">
                                            <a href="#"> Speed Orbital Jigsaw </a>
                                            <p class="price"> $215.00</p>

                                        </div>
                                        <div class="cart-grid-box-del">
                                            <a href="#"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="cart-grid-box">
                                        <div class="cart-grid-box-img">
                                            <img class="img-responsive" src="images/shop/cart.png" alt="">
                                        </div>
                                        <div class="cart-grid-box-title">
                                            <a href="#"> Speed Orbital Jigsaw  </a>
                                            <p class="price"> $215.00</p>
                                        </div>
                                        <div class="cart-grid-box-del">
                                            <a href="#"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="total-amount fix">
                                        <p>SUBTOTAL: </p><span>$330.00 </span>

                                    </div>
                                    <div class="action-cart">
                                        <a href="#" class="viewcart">View Cart</a>
                                        <a href="#" class="checkout">Check Out</a>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Menu Section -->
        <div class="navigation-2">
            <!-- navigation-start -->
            <nav class="navbar navbar-default ">
                <div class="container edit2">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header gap edit3">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                         <a href="index.php"><img class="distance" src="images/demo4.png" alt=""></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="main-navigation">
                        <ul class="nav navbar-nav">
                           <li class="active"><a href="index.php">Home</a></li>
                             <li><a href="about.php">About </a> </li>
                               <li><a href="services.php">Services</a> </li>
                            </li>
                              <li><a href="equipment.php">Equipment</a> </li>
                              <li><a href="contact.php">Contact us</a></li>
                            <!-- <li class="right-slide hidden-xs" id="slideBotton"><i class="fa fa-bars" aria-hidden="true"></i></li> -->
                        </ul>
                        <a href="contact.php" class="btn btn-primary pull-right"><span class="hidden-sm">Request A </span>Quote</a> 
                        
                        </div>
                    <!-- /.navbar-collapse -->

                </div>
                <!-- /.container-end -->
            </nav>
        </div>
        <!-- /.navigation-end -->
        <!-- Menu  End -->
    </header>
    <!-- =-=-=-=-=-=-= HEADER END =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= HOME SLIDER =-=-=-=-=-=-= -->
    <section class="slider-container">
        <div class="slider-grids">
    
            <ul>
                <!-- SLIDE  -->
                <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Truly Creative">

                    <!-- MAIN IMAGE -->
                    <img src="images/slider/slide1.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tt-slider-title  sft sft1" data-x="20" data-y="200" data-speed="1000" data-start="500" data-easing="Power4.easeOut" data-endspeed="300" style="z-index: 999; left:550px !important;">Discount Crane<span>Services</span> </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tt-slider-subtitle  sft sft2" data-x="20" data-y="300" data-speed="1000" data-start="700" data-easing="Power4.easeOut" data-endspeed="300" style="z-index: 999"> Crane services provider </div>

                    <!-- LAYER NR. 3 -->
                    <!-- <div class="tp-caption tt-slider-small-text sft " data-x="20" data-y="370" data-speed="1000" data-start="900" data-easing="Power4.easeOut" data-endspeed="300" style="z-index: 999"> </div> -->

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption sft sft3" data-x="20" data-y="450" data-speed="1000" data-start="1100" data-easing="Power4.easeOut" data-endspeed="300" style="z-index: 999"> <a href="services.php" class="tt-btn btn-bordered light">View Services</a> </div>
                </li>

                <!-- SLIDE  -->
                <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Truly Creative">

                    <!-- MAIN IMAGE -->
                    <img src="images/slider/slide2.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tt-slider-title sft sft1"
                     data-x="20"
                     data-y="200"
                     data-speed="1000"
                     data-start="500"
                     data-easing="easeOutBack"
                     data-endspeed="300"
                     style="z-index: 999">Discount Crane<span>Services</span>
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tt-slider-subtitle sft sft2"
                     data-x="20" data-y="300"
                     data-speed="1000"
                      data-start="700"
                       data-easing="parallaxtoright"
                        data-endspeed="300"
                         style="z-index: 999"> Crane services provider </div>

                    <!-- LAYER NR. 3 -->
              <!--       <div class="tp-caption tt-slider-small-text sft" data-x="20" data-y="370" data-speed="1000" data-start="900" data-easing="Power4.easeOut" data-endspeed="300" style="z-index: 999"> </div> -->

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption sft sft3" data-x="20" data-y="450" data-speed="1000" data-start="1100" data-easing="Power4.easeOut" data-endspeed="300" style="z-index: 999"> <a href="services.php" class="tt-btn btn-bordered light">View services</a> </div>
                </li>
                <!-- SLIDE  -->
                <!-- SLIDE  -->
                
            </ul>
        </div>
    </section>
    <!--======= HOME SLIDER END =========-->



    <!-- =-=-=-=-=-=-= Services Tabs =-=-=-=-=-=-= -->
    <section class="home-tabs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-container tab-upp">
                        <ul role="tablist" class="nav nav-tabs upp">
                            <li class="col-xs-3 clearfix active">
                                <a data-toggle="tab" role="tab" href="#construction" aria-expanded="false">
                                    <div class="tab-image">
                                    	<div class="icon flaticon-buildings"></div>
                                    </div>
                                    <span class="hidden-xs">Construction</span>
                                </a>
                            </li>
                            <li class="col-xs-3 clearfix">
                                <a data-toggle="tab" role="tab" href="#renovations" aria-expanded="false">
                                    <div class="tab-image">
                                        <div class="icon flaticon-drilling-wall"></div>
                                    </div>
                                    <span class="hidden-xs">Jack-and-Slide</span>
                                </a>
                            </li>
                            <li class="col-xs-3 clearfix">
                                <a data-toggle="tab" role="tab" href="#plumbing" aria-expanded="false">
                                    <div class="tab-image clearfix">
                                    	<div class="icon flaticon-pipes"></div>
                                    </div>
                                    <span class="hidden-xs">Odds & Ends </span>
                                </a>
                            </li>
                            <li class="col-xs-3 clearfix active">
                                <a data-toggle="tab" role="tab" href="#architecture" aria-expanded="false">
                                    <div class="tab-image">
                                        <div class="icon flaticon-construction-3"></div>
                                    </div>
                                    <span class="hidden-xs">Oilfield: Building</span>
                                </a>
                            </li>



                        </ul>

                        <div class="tab-content">
                            <!-- tab content  -->
                      
                            <div id="construction" class="row tab-pane active in fade">
                                <div class="col-md-12">
                                          <div class="main-heading text-center">
                <h2>Construction</h2>
            </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="gap">
                                       <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Steel erection</p>
<p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Air units</p>
<p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Roof's /truss's</p>
<p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Tower crane rig up and down</p>
                                    <br>
                                    <a class="btn btn-primary" href="contact.php"> Contact Us</a> </div></div>
                                <div class="col-md-8 col-sm-12 col-xs-12"> <img alt="" src="images/services/ser1.jpg" class="img-responsive center-block"> </div>
                            </div>

                            <!-- tab content -->
                            <div id="renovations" class="row tab-pane fade ">
                                <div class="col-md-12">
                                          <div class="main-heading text-center">
                <h2>Jack-and-Slide</h2>
            </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                               <div class="gap">
                                    <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Load</p>
                                         <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Offload</p>
                                         <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Weigh heavy loads</p>
                                    <br>
                                    <a class="btn btn-primary" href="contact.php">Contact Us</a> </div></div>
                                <div class="col-md-8 col-sm-12 col-xs-12"> <img alt="" src="images/services/ser2.jpg" class=" center-block img-responsive"> </div>
                            </div>

                            <!-- tab content  -->
                            <div id="plumbing" class="row tab-pane fade">
                                <div class="col-md-12">
                                          <div class="main-heading text-center">
                <h2>Odds & Ends </h2>
            </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="gap">
                                   <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Car drops (extreme annihilation)</p>

<p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Stage erection (Big Valley)</p>
<p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Machine moving</p>
<p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Airport improvement & cargo unloading</p>
                                    <br>
                                    <a class="btn btn-primary " href="contact.php"> Contact Us</a> </div></div>
                                <div class="col-md-8 col-sm-12 col-xs-12"> <img alt="" src="images/services/ser3.jpg" class="img-responsive center-block"> </div>
                            </div>

                            <!-- tab content  -->
                            <div id="architecture" class="row tab-pane fade">
                                <div class="col-md-12">
                                          <div class="main-heading text-center">
                <h2>Oilfield: Rig Moving / Building</h2>
            </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="gap">
                                    <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Plant maintenance</p>
                                         <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Mods and buildings</p>
                                          <p><i class="fa fa-hand-o-right styl" aria-hidden="true"></i>  Tank loading and setting</p>
                                    <br>
                                    <a class="btn btn-primary" href="contact.php"> Contact Us</a> </div></div>
                                <div class="col-md-8 col-sm-12 col-xs-12 "> <img alt="" src="images/services/ser4.jpg" class="img-responsive center-block"> </div>
                            </div>
                        </div>

                        <!-- End Tab panes -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= Services Tabs End =-=-=-=-=-=-= -->




    <!-- =-=-=-=-=-=-= Gallery =-=-=-=-=-=-= -->
   
    <!-- =-=-=-=-=-=-= Gallery End =-=-=-=-=-=-= -->
    
        <!-- =-=-=-=-=-=-= About us =-=-=-=-=-=-= -->
 <section>
     <div class="container">
        <div class="main-heading text-center edit">
                <h2>About us</h2>
            </div>
         <div class="row">
              <div class="col-md-6">

                                <div class="about-title about">
                                
                                    <p>Discount Crane Service (A Division of Dacapa Crane & Rigging Ltd.) was established in Alberta in 1996 to provide hoisting, rigging, lift planning and transportation services for the mining, oil & gas, construction and power generation industries.</p>

<p>Discount Crane Service is a privately-owned, Alberta-based company which has built its foundation on consistently providing complete customer satisfaction through a team based attitude and working closely with the client to safely and cost effectively complete their projects. </p>
<div class="about-separate">
                                            <p>Our level of service and commitment starts with the first phone call and continues on through to the completion of the job. We provide modern, clean, affordable and professionally maintained equipment to cover all of the diverse hoisting our customers require.</p> </div>
</div>
</div>


                                   
                                        <!-- grid -->
                                        <div class="col-md-6">
                                            <div class="service-image margin-bottom-30">
                                                <a href="#"><img alt="" class="img-responsive" src="images/services/paint1.jpg"></a>
                                            </div>

                                        </div>
                                        <!-- grid end -->


                                        <div class="clearfix"></div>



                                    </div>


                              
         </div>
     
 </section>
    <!-- =-=-=-=-=-=-= Our Services-end =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Call To Action =-=-=-=-=-=-= -->
    <div class="parallex-small coloor">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="parallex-text">
                        <h4>Not sure which solution fits you business needs?</h4>
                    </div>
                    <!-- end subsection-text -->
                </div>
                <!-- end col-md-8 -->

                <div class="col-md-4 col-sm-4">
                    <div class="parallex-button"> <a class="btn btn-primary" href="#"> Get a Qoute</a> </div>
                    <!-- end parallex-button -->
                </div>
                <!-- end col-md-4 -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- =-=-=-=-=-=-= Call To Action End =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Our Team =-=-=-=-=-=-= -->
    
    <!-- =-=-=-=-=-=-= Our Team End =-=-=-=-=-=-= -->

	<!-- =-=-=-=-=-=-= FunFacts =-=-=-=-=-=-= -->
  
	<!-- =-=-=-=-=-=-= FunFacts End =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Blog & News =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= -->
    

    <!-- =-=-=-=-=-=-= Our Testimonials =-=-=-=-=-=-= -->
  
    <!-- =-=-=-=-=-=-= Our Services-end =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Download Our App =-=-=-=-=-=-= -->
    <section class="section-padding app-downloads parallex" data-stellar-background-ratio="0.1">
        <div class="container">
        <div class="main-heading text-center heading">
                <h2>Our Equipment</h2>
            </div>
            <!-- End title-section -->

            <!-- Row -->
            <div class="row">
                <!-- Team Grid -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="wajhi img-responsive" src="images/team/img3.jpg">
                            <div class="team-grid-overlay">
                                <!-- <div class="social-media">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                </div> -->
                            </div>
                        </div>
                        <div class="team-content">
                            <h2>Carrydeck Cranes</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <!-- Team Grid End-->
                <!-- Team Grid -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="wajhi img-responsive" src="images/team/img4.jpg">
                            <div class="team-grid-overlay">
                               <!--  <div class="social-media">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                </div> -->
                            </div>
                        </div>
                        <div class="team-content">
                            <h2>Spyder Cranes</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <!-- Team Grid End-->
                <!-- Team Grid -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="wajhi img-responsive" src="images/team/img5.jpg">
                            <div class="team-grid-overlay">
                                <!-- <div class="social-media">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                </div> -->
                            </div>
                        </div>
                        <div class="team-content">
                            <h2>Picker Trucks</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <!-- Team Grid End-->
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-=  Download Our App End  =-=-=-=-=-=-= -->
    
    
    <!-- =-=-=-=-=-=-= Our Clients =-=-=-=-=-=-= -->
     <div class="parallex-small ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="parallex-text">
                        <h4>Not sure which solution fits you business needs?</h4>
                    </div>
                    <!-- end subsection-text -->
                </div>
                <!-- end col-md-8 -->

                <div class="col-md-4 col-sm-4">
                    <div class="parallex-button"> <a href="contact.php" class="btn btn-primary">Contact Us <i class="fa fa-angle-double-right "></i></a> </div>
                    <!-- end parallex-button -->
                </div>
                <!-- end col-md-4 -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- =-=-=-=-=-=-= Our Clients -end =-=-=-=-=-=-= -->

       <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
    <footer>
        <div class="footer light-color custom-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">About us</h3>
						
                        <p>Discount Crane Service (A Division of Dacapa Crane & Rigging Ltd.) was established in Alberta in 1996 to provide hoisting, rigging, lift planning and transportation services for the mining, oil & gas, construction and power generation industries.</p>
                        <div class="social-links-two clearfix">
                         <a class="facebook img-circle" href="#"><span class="fa fa-facebook-f"></span></a>
                         <a class="twitter img-circle" href="#"><span class="fa fa-twitter"></span></a>
                         <a class="google-plus img-circle" href="#"><span class="fa fa-google-plus"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-pinterest-p"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-linkedin"></span></a>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-2 col-sm-6 col-xs-12 clearfix column">
                       <!--  <h3 class="footer-title">Quick links</h3>
                        <div class="footer-links">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                
                                <li><a href="about.php">About</a></li>
                                
                                <li><a href="services.php">Service</a></li>
                                
                                <li><a href="equipment.php">Equipment</a></li>
                               
                                <li><a href="contact.php">Contact</a></li>
                                
                            </ul>
                        </div> -->
                    </div>
                    <!-- /.col -->
                   <!--  <div class="col-md-3 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">Flickr widget</h3>
                        <div class="footer-gallery-grid">
                            <a href="#">
                                <img alt="" src="images/blog/small-1.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-2.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-3.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-4.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-5.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-6.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-7.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-8.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-9.png">
                            </a>
                            
                       
                        </div>
                    </div> -->
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">Contact us</h3>
                        <div class="footer-address">
                            <p><i class="fa fa-map-marker"></i>23 CreditStone Rd, Concord, ON L4K 1N4, CA</p>
                            <p><i class="fa fa-envelope"></i>discounttowing@hotmail.com</p>
                            <p><i class="fa fa-phone"></i>416-676-5555</p>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.footer -->
        <div class="copyright light-copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <p>Copyright &copy; 2019 Discount Crane &nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<!-- =-=-=-=-=-=-= FOOTER END =-=-=-=-=-=-= -->
    <!-- Slide Menu Section Start Here -->
        <div class="menu-info" id="info-panel">
            <div class="close"><i class="fa fa-times" aria-hidden="true"></i></div>
            <ul>                
                <li><i class="fa fa-globe" aria-hidden="true"></i><span> English </span>
                    <ul>
                        <li><a href="#">English</a></li>
                        <li><a href="#">French</a></li>
                        <li><a href="#">Aribic</a></li>
                    </ul>
                  <span> <i class="fa fa-angle-down" aria-hidden="true"></i></span>
                </li>
                <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i>416-676-5555 </a></li>
                <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> discounttowing@hotmail.com</a></li>
                <li><a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> Monday - Saturday</a></li>
            </ul>
        </div>
        <!-- Slide Menu Section EndHere -->
    <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap Core Css  -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Menu Hover  -->
    <script src="js/hover.min.js"></script>
    <!-- Jquery Smooth Scroll  -->
    <script src="js/jquery.smoothscroll.js"></script>
    <!-- Jquery Easing -->
    <script type="text/javascript" src="js/easing.js"></script>
    <!-- Jquery Counter -->
    <script src="js/jquery.countTo.js"></script>
    <!-- Jquery Waypoints -->
    <script src="js/jquery.waypoints.js"></script>
    <!-- Jquery Appear Plugin -->
    <script src="js/jquery.appear.min.js"></script>
    <!-- Jquery Shuffle Portfolio -->
    <script src="js/jquery.shuffle.min.js"></script>
    <!-- Carousel Slider  -->
    <script src="js/carousel.min.js"></script>
    <!-- Jquery Parallex -->
    <script src="js/jquery.stellar.min.js"></script>
    
    <!-- jQuery REVOLUTION Slider  -->
    <script src="js/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="js/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Gallery Magnify  -->
    <script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Sticky Bar  -->
    <script src="js/theia-sticky-sidebar.js"></script>
    <!-- Template Core JS -->
    <script src="js/custom.js"></script>
   <!-- =-=-=-=-=-=-= JQUERY End =-=-=-=-=-=-= -->
   
   <script type="text/javascript">
   
   (function($) {
    "use strict";
   
   var vid = document.getElementById("video-bg");
	var pauseButton = document.querySelector("#play-video button");

	function vidFade() {
	    vid.classList.add("stopfade");
	}
	vid.addEventListener('ended', function () {
	    // only functional if "loop" is removed 
	    vid.pause();
	    // to capture IE10
	    vidFade();
	});
	pauseButton.addEventListener("click", function () {
	    vid.classList.toggle("stopfade");
	    if (vid.paused) {
	        vid.play();
	        pauseButton.innerHTML = [
	"<i class='fa fa-pause'></i>"
	];
	    } else {
	        vid.pause();
	        pauseButton.innerHTML = [
	"<i class='fa fa-play'></i>"
	];
	    }
	});
	
	})(jQuery);
	</script>
   
</body>


<!-- Mirrored from templates.scriptsbundle.com/crane-template/demos/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 08:12:42 GMT -->
</html>