﻿<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from templates.scriptsbundle.com/crane-template/demos/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 08:22:39 GMT -->
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!--[if IE]>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="author" content="ScriptsBundle">
    <title>Crane Construction Template</title>
    <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/style.css">
    <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">
    <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/et-line-fonts.css" type="text/css">
    <!-- =-=-=-=-=-=-= Animation =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">
    <!-- =-=-=-=-=-=-= Menu Drop Down =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="css/bootstrap-dropdownhover.min.css" type="text/css">
    <!-- =-=-=-=-=-=-= Revolution Main Stylesheet =-=-=-=-=-=-= -->
    <link href="js/revolution/css/settings.css" rel="stylesheet" media="screen" />
    <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.style.css">
    <!-- =-=-=-=-=-=-= Google Fonts =-=-=-=-=-=-= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic,900italic,900,300,300italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
    <!-- =-=-=-=-=-=-= Magnific PopUP CSS =-=-=-=-=-=-= -->
	<link href="js/magnific-popup/magnific-popup.css" rel="stylesheet">
	<!-- =-=-=-=-=-=-= Flaticons CSS =-=-=-=-=-=-= -->
	<link href="css/flaticon.css" rel="stylesheet">
    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="js/revolution/css/settings.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->
  

    <!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->
    <div class="header-top clear">
        <div class="container">
            <div class="row">
                <div class="col-md-7 hidden-sm hidden-xs">
                    <div class="header-top-left header-top-info">
                         <p><a href="tel:+3211234567"><i class="fa fa-phone"></i> 416-676-5555</a></p>
                         <p><a href="mailto:contact@scriptsbundle.com"><i class="fa fa-envelope"></i>discounttowing@hotmail.com</a></p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="header-top-right pull-right">
                        <ul>
                            <!-- <li class="search-bar">
                                <div class="search-form">
                                    <form action="#" method="get">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                        <input type="text" name="search" id="search" placeholder="Search information">
                                    </form>
                                </div>
                            </li> -->
                           <!--    <div class="social-links-two clearfix">
                         <a class="facebook img-circle" href="#"><span class="fa fa-facebook-f"></span></a>
                         <a class="twitter img-circle" href="#"><span class="fa fa-twitter"></span></a>
                         <a class="google-plus img-circle" href="#"><span class="fa fa-google-plus"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-pinterest-p"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-linkedin"></span></a>
                        </div> -->
                          <!--   <li class="cart-active">
                                <div class="cart-btn">
                                    <a href="#"><i class="fa fa-shopping-cart"></i><sup>2</sup></a>
                                </div>
                                <div class="cart-grid">
                                    <div class="cart-grid-box">
                                        <div class="cart-grid-box-img">
                                            <img class="img-responsive" src="images/shop/cart.png" alt="">
                                        </div>
                                        <div class="cart-grid-box-title">
                                            <a href="#"> Speed Orbital Jigsaw </a>
                                            <p class="price"> $215.00</p>

                                        </div>
                                        <div class="cart-grid-box-del">
                                            <a href="#"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="cart-grid-box">
                                        <div class="cart-grid-box-img">
                                            <img class="img-responsive" src="images/shop/cart.png" alt="">
                                        </div>
                                        <div class="cart-grid-box-title">
                                            <a href="#"> Speed Orbital Jigsaw  </a>
                                            <p class="price"> $215.00</p>
                                        </div>
                                        <div class="cart-grid-box-del">
                                            <a href="#"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="total-amount fix">
                                        <p>SUBTOTAL: </p><span>$330.00 </span>

                                    </div>
                                    <div class="action-cart">
                                        <a href="#" class="viewcart">View Cart</a>
                                        <a href="#" class="checkout">Check Out</a>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Top End  -->

    <!-- Header Area -->
    <header class="header-area">

        <!-- Logo Bar -->
        <div class="border">
            <!-- Logo Bar -->
            <div class="logo-bar">
                <div class="container clearfix">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="index.php"><img src="images/demo5.png" alt=""></a>
                    </div>

                    <!--Info Outer-->
                    <div class="information-content">
                        <!--Info Box-->
                        <div class="info-box  hidden-sm">
                            <div class="icon"><span class="icon-envelope"></span></div>
                            <div class="text">Email</div>
                            <a href="mailt:contact@scriptsbundle.com">discounttowing@hotmail.com</a> </div>
                        <!--Info Box-->
                        <div class="info-box">
                            <div class="icon"><span class="icon-phone"></span></div>
                            <div class="text">Call Now</div>
                            <a class="location" href="#"> 416-676-5555</a> </div>
                        <div class="info-box">
                            <div class="icon"><span class="icon-map-pin"></span></div>
                            <div class="text">Address</div>
                            <a class="location" href="#">23 CreditStone Rd, Concord, ON L4K 1N4, CA </a> </div>
                    </div>
                </div>
            </div>
            <!-- Header Top End -->
        </div>
        <!-- Header Top End -->

        <!-- Menu Section -->
        <div class="navigation-2">
            <!-- navigation-start -->
            <nav class="navbar navbar-default ">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="main-navigation">
                         <ul class="nav navbar-nav">
                           <li><a href="index.php">Home</a></li>
                             <li class="active"><a href="about.php">About </a> </li>
                               <li><a href="services.php">Services</a> </li>
                            </li>
                              <li><a href="equipment.php">Equipment</a> </li>
                              <li><a href="contact.php">Contact us</a></li>
                           
                        </ul>
                        <a href="contact.php" class="btn btn-primary pull-right">Request A Quote</a> </div>
                    <!-- /.navbar-collapse -->

                </div>
                <!-- /.container-end -->
            </nav>
        </div>
        <!-- /.navigation-end -->
        <!-- Menu  End -->
    </header>
    <!-- =-=-=-=-=-=-= HEADER END =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Page Breadcrumb =-=-=-=-=-=-= -->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <!-- end col -->
                <div class="col-md-12 col-sm-12 text-left">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Pages</a></li>
                            <li><a href="#" class="active">About</a></li>
                        </ol>
                    </div>
                    <!-- end bread -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Page Breadcrumb End =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= About Us =-=-=-=-=-=-= -->
    <section id="about" class="padding-top-80">
        <div class="container">
           <div class="main-heading text-center">
                <h2>About Us</h2>
            </div>
            <!-- Row -->
            <div class="row">
                <!-- Team Grid -->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="about-title">
                        <h2>Need a Crane In Calgary? Edmonton? Fort McMurray? We Have You Covered!</h2>
                        <p>Discount Crane Service (A Division of Dacapa Crane & Rigging Ltd.) was established in Alberta in 1996 to provide hoisting, rigging, lift planning and transportation services for the mining, oil & gas, construction and power generation industries.</p>
                        <p>Discount Crane Service is a privately-owned, Alberta-based company which has built its foundation on consistently providing complete customer satisfaction through a team based attitude and working closely with the client to safely and cost effectively complete their projects.</p>
                        <p>Our level of service and commitment starts with the first phone call and continues on through to the completion of the job. We provide modern, clean, affordable and professionally maintained equipment to cover all of the diverse hoisting our customers require.</p>
                        <p>Discount Crane Service is capable of mobilizing equipment and rigging throughout Alberta to suit all project sizes, and focuses on the highest quality hoisting services to ensure customer satisfaction, while maintaining high standards with regards to health, safety and the environment.</p>
                        <p>Safety is an essential element of Discount Crane Service. We are committed to protecting our employees, contractors, visitors, and the public from harm and our resources from loss. In fulfilling our commitment to protect both people and property, Discount management will provide and maintain a safe and healthy work environment in accordance with legislated, industry and client standards.</p>
                        <p>We are proud of our safety success and our goal of zero lost time incidents. We are members of the Partnerships in Injury Reduction Program and hold a COR certificate through the Alberta Construction Safety Association. As well, we are a member of ISNetworld, PICS, CQ Network, Comply Works and Browz.</p>
                       <!--  <ul class="custom-links">
                            <li><a href="#">Nullam placerat ante ac est consequat, sed laoreet enim porta.</a></li>
                            <li><a href="#">Morbi gravida dolor quis orci pharetra, eu vestibulum nibh rutrum.</a></li>
                            <li><a href="#">Sed aliquet erat sed lacus molestie, id ullamcorper neque rhoncus.</a></li>
                            <li><a href="#">Nullam tempor elit quis dui lacinia, aliquam rutrum nulla congue.</a></li>
                            <li><a href="#">Pellentesque a dui eget urna rhoncus convallis at at libero.</a></li>
                            <li><a href="#">Curabitur semper tortor ac augue iaculis mattis.</a></li>
                            <li><a href="#">Praesent laoreet ex vitae laoreet vehicula.</a></li>
                            <li><a href="#">Donec a nulla facilisis ipsum accumsan sagittis et id mi.</a></li>
                        </ul> -->
                    </div>
                </div>
                <!-- Team Grid End-->

                <!-- Team Grid -->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="about-image">
                        <img class="" alt="" src="images/Crane1.png">
                    </div>
                </div>
                <!-- Team Grid End-->
            </div>
            <!-- Row End -->
            <div class="row">
                <div class="about-services margin-top-20">
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <!-- services grid -->
                        <div class="services-grid-4">
                          <!--   <img alt="" src="images/icons/hammer.png"> -->
                            <div class="content-area">
                                <h4>PRODUCTS</h4>
                                <p>Discount crane services has hundreds of equipment items ready for you. Click to view our online catalog. Spec sheets and equipment details can be found on each equipment page.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <!-- services grid -->
                        <div class="services-grid-4">
                           <!--  <img alt="" src="images/icons/paint-brush.png"> -->
                            <div class="content-area">
                                <h4>PORTFOLIO</h4>
                                <p>Discount crane services has worked with major projects across Western Canada. With projects including the setup of wind turbines, to large-scale structure development, Discount crane services is able to take on multi-industry developments.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <!-- services grid -->
                        <div class="services-grid-4">
                            <!-- <img alt="" src="images/icons/driller.png"> -->
                            <div class="content-area">
                                <h4>CONTACT US</h4>
                                <p>Do you have questions, concerns, comments? Get in touch with us today. Our team of professionals can answer any questions you may have.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= About Us =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= PARALLEX =-=-=-=-=-=-= -->
   <!--  <div class="parallex  quote-bg  section-padding-100" data-stellar-background-ratio="0">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="parallex-text">
                        <h4>IMPRESSED WITH US ? </h4>
                        <p>Fusce non leo ut sapien volutpat volutpat tiam rutrum malesuada erat sollicitudin non. </p>
                    </div>
                    end subsection-text
                </div>
                end col-md-8

                <div class="col-md-4">
                    <div class="parallex-button margin-top-20"> <a href="#" class="section-scroll btn btn-lg btn-clean">Contact Us</a> </div>
                    end parallex-button
                </div>
            </div>
            
        </div>
        
    </div> -->
    <!-- =-=-=-=-=-=-= PARALLEX END =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Our Team =-=-=-=-=-=-= -->
    
    <!-- =-=-=-=-=-=-= Our Team End =-=-=-=-=-=-= -->
    
    <!-- =-=-=-=-=-=-= Our Clients =-=-=-=-=-=-= -->
    <div class="parallex-small ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="parallex-text">
                        <h4>Not sure which solution fits you business needs?</h4>
                    </div>
                    <!-- end subsection-text -->
                </div>
                <!-- end col-md-8 -->

                <div class="col-md-4 col-sm-4">
                    <div class="parallex-button"> <a href="contact.php" class="btn btn-lg btn-clean">Contact Us <i class="fa fa-angle-double-right "></i></a> </div>
                    <!-- end parallex-button -->
                </div>
                <!-- end col-md-4 -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- =-=-=-=-=-=-= Our Clients -end =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
    <footer>
        <div class="footer custom-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">About us</h3>
                        
                        <p>Discount Crane Service (A Division of Dacapa Crane & Rigging Ltd.) was established in Alberta in 1996 to provide hoisting, rigging, lift planning and transportation services for the mining, oil & gas, construction and power generation industries.</p>
                        <div class="social-links-two clearfix">
                         <a class="facebook img-circle" href="#"><span class="fa fa-facebook-f"></span></a>
                         <a class="twitter img-circle" href="#"><span class="fa fa-twitter"></span></a>
                         <a class="google-plus img-circle" href="#"><span class="fa fa-google-plus"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-pinterest-p"></span></a>
                         <a class="linkedin img-circle" href="#"><span class="fa fa-linkedin"></span></a>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-2 col-sm-6 col-xs-12 clearfix column">
                       <!--  <h3 class="footer-title">Quick links</h3>
                        <div class="footer-links">
                            <ul>
                               
                                <li><a href="index.html">Home</a></li>
                                
                                <li><a href="about.html">About</a></li>
                                
                                <li><a href="services.html">Service</a></li>
                                
                                <li><a href="equipment.html">Equipment</a></li>
                               
                                <li><a href="contact.html">Contact</a></li>
                                
                            </ul>
                        </div> -->
                    </div>
                    <!-- /.col -->
                   <!--  <div class="col-md-3 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">Flickr widget</h3>
                        <div class="footer-gallery-grid">
                            <a href="#">
                                <img alt="" src="images/blog/small-1.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-2.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-3.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-4.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-5.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-6.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-7.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-8.png">
                            </a>
                            <a href="#">
                                <img alt="" src="images/blog/small-9.png">
                            </a>
                            
                       
                        </div>
                    </div> -->
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12 clearfix column">
                        <h3 class="footer-title">Contact us</h3>
                        <div class="footer-address">
                            <p><i class="fa fa-map-marker"></i>23 CreditStone Rd, Concord, ON L4K 1N4, CA</p>
                            <p><i class="fa fa-envelope"></i>discounttowing@hotmail.com</p>
                            <p><i class="fa fa-phone"></i>416-676-5555</p>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.footer -->
         <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <p>Copyright &copy; 2019 Discount Crane &nbsp;</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<!-- =-=-=-=-=-=-= FOOTER END =-=-=-=-=-=-= -->
    
    <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap Core Css  -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Menu Hover  -->
    <script src="js/hover.min.js"></script>
    <!-- Jquery Smooth Scroll  -->
    <script src="js/jquery.smoothscroll.js"></script>
    <!-- Jquery Easing -->
    <script type="text/javascript" src="js/easing.js"></script>
    <!-- Jquery Counter -->
    <script src="js/jquery.countTo.js"></script>
    <!-- Jquery Waypoints -->
    <script src="js/jquery.waypoints.js"></script>
    <!-- Jquery Appear Plugin -->
    <script src="js/jquery.appear.min.js"></script>
    <!-- Jquery Shuffle Portfolio -->
    <script src="js/jquery.shuffle.min.js"></script>
    <!-- Carousel Slider  -->
    <script src="js/carousel.min.js"></script>
    <!-- Jquery Parallex -->
    <script src="js/jquery.stellar.min.js"></script>
    
    <!-- jQuery REVOLUTION Slider  -->
    <script src="js/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="js/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Gallery Magnify  -->
    <script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Sticky Bar  -->
    <script src="js/theia-sticky-sidebar.js"></script>
    <!-- Template Core JS -->
    <script src="js/custom.js"></script>
   <!-- =-=-=-=-=-=-= JQUERY End =-=-=-=-=-=-= -->
</body>


<!-- Mirrored from templates.scriptsbundle.com/crane-template/demos/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 08:22:39 GMT -->
</html>